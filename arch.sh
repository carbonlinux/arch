#!/bin/bash
#
# Arch Install guide for (HP 15-p001tx Notebook Laptop)
#

# PING for Network Testing 
ping -c 3 google.com
sleep 2

# Partition Table 
cfdisk /dev/sda

# Mount Partition
mkdir /mnt
mount /dev/sda1 /mnt
mkdir /mnt/home
mount /dev/sda2 /mnt/home

# Mirror Update
#nano /etc/pacman.d/mirrorlist  //remove # symbol to update mirrorlist

# Installing arch based system
pastrap -i /mnt base

# Fstab Update
genfstab -U -p /mnt >> /mnt/etc/fstab
# To update fstab
#nano /mnt/etc/fstab

# arch-chroot 
arch-chroot /mnt

# Location
#nano /etc/locale.gen

echo LANG=en_US.UTF-8 > /etc/locale.conf
export LANG=en_US.UTF-8

# 
# zone update
ls /usr/share/zoneinfo
ln -s /usr/share/zoneinfo/America/Los_Angeles /etc/localtime

#
hwclock --systohc --utc
#
# Setting up network for linux
systemctl enable dhcpcd@eth0.service
pacman -S wireless_tools wpa_supplicant wpa_actiond dialog
wifi-menu
systemctl enable net-auto-wireless.service

#
# Configure Your Package Manager
echo -e "[multilib] \n Include = /etc/pacman.d/mirrorlist" >> /etc/pacman.conf

# Sync 
pacman -Sy

# password
passwd

# Add user 
useradd -m -g users -G wheel,storage,power -s /bin/bash  arch
passwd arch

#
pacman -S sudo
pacman -Ss sudo

echo "%wheel ALL=(ALL) ALL" >> /etc/sudoers

#
pacman -S grub-bios
grub-install --target=i386-pc --recheck /dev/sda
cp /usr/share/locale/en\@quot/LC_MESSAGES/grub.mo /boot/grub/locale/en.mo
pacman -S os-prober
#
# 
grub-mkconfig -o /boot/grub/grub.cfg
#
# Exit and unmount system

exit
umount /mnt/home
umount /mnt
reboot


